import '../node_modules/normalize.css/normalize.css';
import 'Svendor/grid.css';
import 'Svendor/container.css';
import 'Sbase/base.scss';
import './scss/templates/agenda.scss';

import './js/components/menu';
import './js/components/footer';
import { q } from './js/commons/helpers';

import { tns } from '../node_modules/tiny-slider/src/tiny-slider';
import '../node_modules/tiny-slider/src/tiny-slider.scss';

const slideOne = '.start-slider';
const $slideOne = q(slideOne);

const leftIcon = '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAASFBMVEUAAADu7u719fX////v7+/v7+/v7+/v7+/t7e309PTt7e3v7+/v7+/u7u7v7+/r6+vu7u7u7u7v7+/u7u7////29vb8/Pzw8PApTRtPAAAAE3RSTlMA9wYD+syk3zkXDq3q020zxJlRo55hDQAAAPJJREFUaN7t2c0SgjAMBOC0jYItPyptef83FZSZHDwvU2P24PWbJTtygCyWthMC4SKEJ2zwxA7ERzdOhIwnf8ml5InkLhDDsSsd2liWZR2iVAEZLie8UbsbCAlg47uHpy1mmGGGGWaY0YQRlPQw4/8M264ZW37SCGcajDTiWN9G6eJhAJCQMu/IOswI5FDomj9VuMcqDFakiytOkcK9hutremJ84l0cuouiu+hbsqa72JLbVGzJrS1Z0xNjW3KTd9H3rjxtyYEkqH+YWRRYl3wXBKFgP8tKF3Z1JAlGqbk8MUVEmVICrUsU+UXGU/BksSjOC8XfJ5Kh6q0tAAAAAElFTkSuQmCC">';

const rigthIcon = '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAAYFBMVEUAAADu7u7y8vL19fXl5eXv7+/v7+/v7+/u7u7v7+/w8PDu7u7q6urv7+/v7+/v7+/v7+/u7u7v7+/r6+vu7u7u7u7v7+/u7u7v7+/v7+/u7u7////v7+/29vb8/Pzy8vK4E8r2AAAAGnRSTlMA+QYDCKTf+sw5F/cO+8vqq9NrM9LEsZlxUZo2Da0AAAEtSURBVGje7ZnLcsMgDEUlSE1bbMdJ+hTg/P9ftnGmo0U3yeJ6MNFdeHvmSgdYmCyWluM8OQLH6xcWTy/TxxFL8fRd0pyewF0OcwwBSvHU788iwmDKe+IsAu3i6cQlZFQXtSsUFnAX90dhMIULZzwlXPdiE6uMYibfTTGTq6SEwo3tJZjJt3dp6Lys+FZmM9neStJs1+RGJrbaXlYweSgD/oY57c/D0mXyKIqjvluqxPmtB0EcvXZzWMY17pCM61F5xjKySLwwHC0BMTIDGTorZWx4Vvge8m8f5u4DuNvI+TB3a5wV3N3R3K1oHzGN27+vpKV9mLsVzWrr7voV3kGP3IdCphSh7upvWQxDMV3hEMEM+kql6N2OyvHQffbKQGV3aYSN+0WgGQvCYqk6Pz2jNWrtXNWNAAAAAElFTkSuQmCC">';

if ($slideOne) {
  tns({
    container: slideOne,
    items: 1,
    mouseDrag: true,
    swipeAngle: true,
    speed: 400,
    loop: false,
    center: false,
    autoplay: true,
    navPosition: 'bottom',
    controls: true,
    autoplayButtonOutput: false,
    controlsText: [leftIcon, rigthIcon],
    responsive: {
      480: {
        items: 2,
      },
      769: {
        items: 3,
      },
    },
  });
}

export { leftIcon, rigthIcon };