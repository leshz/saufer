// import '@babel/polyfill';
import '../node_modules/normalize.css/normalize.css';
import 'Svendor/reset.css';
import 'Svendor/grid.css';
import 'Svendor/container.css';
import 'Sbase/base.scss';
import './js/components/menu';
import './js/components/footer';
import './scss/templates/products.scss';
import './js/components/tabs/tabs';
import { all, id, getParents } from './js/commons/helpers';

import { tns } from '../node_modules/tiny-slider/src/tiny-slider';
import '../node_modules/tiny-slider/src/tiny-slider.scss';

const slideOne = '.sld-wrapper';
const slideTwo = '.sld-wrapperalt';

const $slideOne = document.querySelector(slideOne);
const $slideTwo = document.querySelector(slideTwo);

if ($slideOne) {
  const sliderTabOne = tns({
    container: slideOne,
    items: 1,
    mouseDrag: true,
    swipeAngle: true,
    speed: 400,
    center: true,
    autoplay: true,
    navPosition: 'bottom',
    controls: false,
    autoplayButtonOutput: false,
  });
}
if ($slideTwo) {
  const sliderTabTwo = tns({
    container: slideTwo,
    items: 1,
    center: true,
    mouseDrag: true,
    swipeAngle: true,
    speed: 400,
    autoplay: true,
    navPosition: 'bottom',
    controls: false,
    autoplayButtonOutput: false,
  });
}

const form = all('.form-item');
if (form.length > 0) {
  const button = id('form-display-tabOne');
  const buttonb = id('form-display-tabTwo');

  if (button) {
    button.addEventListener('click', (ev) => {
      const container = getParents(ev.target, '.form-container');
      container[0]
        .querySelector('.form-item:nth-child(2)')
        .classList.toggle('active');
    });
  }
  if (buttonb) {
    buttonb.addEventListener('click', (ev) => {
      const container = getParents(ev.target, '.form-container');
      container[0]
        .querySelector('.form-item:nth-child(2)')
        .classList.toggle('active');
    });
  }
}