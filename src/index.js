import '../node_modules/normalize.css/normalize.css';
import 'Svendor/reset.css';
import 'Svendor/grid.css';
import 'Sbase/base.scss';
import './js/components/menu';
import './js/components/footer';
import { configurationObject, mobileConfiguration } from './js/components/sliders/sliders';

const fullPage = $('#fullpage');

const innerW = window.innerWidth;
const screenW = window.screen.availWidth;
const navigatorWidth = innerW <= screenW ? innerW : screenW;

if (navigatorWidth > 500) {
  fullPage.fullpage(configurationObject);
} else {
  fullPage.fullpage(mobileConfiguration);
}

window.matchMedia('(max-width: 500px)').addListener((ev) => {
  if (ev.matches) {
    fullPage.fullpage.destroy('all');
    fullPage.fullpage(mobileConfiguration);
  }
});
window.matchMedia('(min-width: 500px)').addListener((ev) => {
  if (ev.matches) {
    fullPage.fullpage.destroy('all');
    fullPage.fullpage(configurationObject);
  }
});