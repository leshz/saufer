/* eslint-disable no-param-reassign */
import '../node_modules/normalize.css/normalize.css';
import 'Svendor/grid.css';
import 'Svendor/container.css';
import 'Sbase/base.scss';
import './scss/templates/exito.scss';
import './js/components/menu';
import './js/components/footer';
import './js/components/tabs/tabs';

import { leftIcon, rigthIcon } from './agenda';
import Modal from './js/components/modal/modal';
import { all, getParents } from './js/commons/helpers';
import { tns } from '../node_modules/tiny-slider/src/tiny-slider';
import '../node_modules/tiny-slider/src/tiny-slider.scss';

const modal = new Modal();

function makeSlider() {
  tns({
    container: '#modal .slider-wrap',
    items: 2,
    mouseDrag: true,
    swipeAngle: true,
    speed: 400,
    loop: false,
    center: false,
    autoplay: true,
    nav: false,
    controls: true,
    autoplayButtonOutput: false,
    controlsText: [leftIcon, rigthIcon],
    responsive: {
      480: {
        items: 2,
      },
      769: {
        items: 3,
      },
    },
  });
}

function clickOnCard(event) {
  const item = getParents(event.target, '.card-success');
  const modalContent = item[0].querySelector('.card-success-modal-content');
  if (modalContent !== null) {
    let cloneNode = modalContent.cloneNode(true);
    cloneNode.classList.add('active-modal');
    cloneNode = cloneNode.outerHTML;
    modal.open(cloneNode, makeSlider);
  }
}

const allCards = all('.card-success');

for (const card of allCards) {
  card.addEventListener('click', clickOnCard);
}