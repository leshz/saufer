/* eslint-disable no-console */
// eslint-disable-next-line prefer-destructuring
const log = console.log;
const id = document.getElementById.bind(document);
const q = document.querySelector.bind(document);
const all = document.querySelectorAll.bind(document);
function createHtml(HTMLString) {
  const html = document.implementation.createHTMLDocument();
  html.body.innerHTML = HTMLString;
  return html.body.children[0];
}
function getParents(elem, selector) {
  const firstChar = selector.charAt(0);
  const parents = [];
  // Get closest match
  for (; elem && elem !== document; elem = elem.parentNode) {
    if (firstChar === '.') {
      if (elem.classList.contains(selector.substr(1))) {
        parents.push(elem);
      }
    } else if (firstChar === '#') {
      if (elem.id === selector.substr(1)) {
        parents.push(elem);
      }
    } else if (firstChar === '[') {
      if (elem.hasAttribute(selector.substr(1, selector.length - 1))) {
        parents.push(elem);
      }
    }
  }
  if (parents.length === 0) {
    return false;
  }
  return parents;
}

export {
  log, id, q, all, createHtml, getParents,
};
