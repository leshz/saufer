import 'Svendor/jquery.fullpage.css';
import 'Stemplates/slides.scss';

import { id } from '../../commons/helpers';
import config from './configuration';

function getAnchors() {

  const $menu = id('float-menu');
  const anchorsString = [];
  if ($menu) {
    const menuItems = $menu.getElementsByTagName('li');
    for (let item = 0; item < menuItems.length; item += 1) {
      anchorsString.push(menuItems[item].dataset.menuanchor);
    }
  }
  return anchorsString;
}
const configurationObject = config;
const anchors = getAnchors();

configurationObject.anchors = anchors;

const mobileConfiguration = JSON.parse(JSON.stringify(configurationObject));

mobileConfiguration.paddingTop = '10px';

export { configurationObject, mobileConfiguration };
