const config = {
  menu: '#float-menu',
  paddingTop: '50px',
  paddingBottom: '10px',
  fixedElements: '#float-menu, #footer, #menutop',
};

export default config;
