/* eslint-disable no-console */
/* eslint-disable max-len */
/* eslint-disable no-restricted-syntax */
/* Class Modal */
/**
 * @author Jeffer Barragan  <jabarragans@indracompany.com>
 * @version 1.3
 */
/**
 * @class
 * @classdesc Implementacion de Segment para frontend, Se crea un nuevo Objeto de tipo segmentRecharge.
 * @prop {string} base String HTML base en donde se implementara el contenido de la ventana modal
 * @prop {string} selector String Selector para indicar en que elemento del DOM se implementa la ventana modal
 * @prop {int} animationTime Tiempo para la animación de apertura y cierre
 * @prop {int} animationMiliseconds Tiempo para la animación de apertura y cierre en milisegundos
 */
import './modal.scss';

export default class Modal {
  constructor(configurationObject = {}) {
    this.baseClass = configurationObject.baseClass || '';
    this.selector = configurationObject.selector || 'body';
    this.animationTime = configurationObject.animationTime || 1; // Seconds
    this.animationMiliseconds = this.animationTime * 1000;
  }

  open(template, callBackAfterShowModal) {
    const that = this;
    let $ModalContainer = document.getElementById('modal');
    function opening() {
      $ModalContainer.classList.add('active');
      const $Template = that.render(template);
      const animationString = `modalIn ${that.animationTime}s forwards`;
      $Template.style.animation = animationString;
      $ModalContainer.append($Template);
      that.closeEvent();
      if (callBackAfterShowModal) callBackAfterShowModal();
    }
    if (!$ModalContainer) {
      document
        .querySelector(this.selector)
        .append(
          Modal.createDomElement("<div id='modal' class='modal-overlay'></div>"),
        );
    }
    $ModalContainer = document.getElementById('modal');
    if ($ModalContainer.classList.contains('active')) {
      this.close(true);
      setTimeout(() => {
        opening();
      }, that.animationMiliseconds);
    } else {
      opening();
    }
  }

  closeEvent() {
    const that = this;
    try {
      const element = document.querySelector('#modal .close-modal');
      element.addEventListener('click', () => {
        that.close();
      });
    } catch (err) {
      console.info('🔥No Close Modal🔥');
    }
  }

  close(ContinueLoadModal) {
    const $modal = document.getElementById('modal');
    const animationString = `modalOut ${this.animationTime}s forwards`;
    $modal.querySelector('.modal-wrap').style.animation = animationString;
    setTimeout(() => {
      $modal.querySelector('.modal-wrap').remove();
      if (!ContinueLoadModal) $modal.classList.remove('active');
    }, this.animationMiliseconds);
  }

  render(template) {
    const templateToRender = template
        || "Contenido Por Default :D <button class='close-modal'>Cerrar</button>";
    const base = `<div class="modal-wrap ${this.baseClass}">{base}</div>`;
    const render = base.replace('{base}', templateToRender);
    return Modal.createDomElement(render);
  }

  static createDomElement(HTMLString) {
    const html = document.implementation.createHTMLDocument();
    html.body.innerHTML = HTMLString;
    return html.body.children[0];
  }
}
