import MENU from 'Slayout/menu.scss';
import { id, q, getParents } from '../commons/helpers';

const floatMenu = id('float-menu');


function topAdtributteMenu() {
  const navigatorHeight = window.innerHeight;
  const innerW = window.innerWidth;
  const screenW = window.screen.availWidth;
  const navigatorWidth = innerW <= screenW ? innerW : screenW;
  const elementHeight = floatMenu.offsetHeight;

  if (navigatorWidth > 500) {
    const result = (navigatorHeight / 2) - (elementHeight / 2);
    floatMenu.style.top = `${result}px`;
  } else {
    floatMenu.style.top = '';
  }
}
if (floatMenu) {
  topAdtributteMenu();
  window.addEventListener('resize', topAdtributteMenu);
}

const burger = id('burger');
if (burger) {
  burger.addEventListener('click', () => {
    burger.querySelector('.toggle').classList.toggle('active');
    q('.animation-mobile').classList.toggle('active');
  });
}

/**
 * Funcion encargada dentro de evento Click para interatividad de menu principal mobile
 * @function
 * @param  {object} event handler de evento
 */
function addActiveMenu(event) {
  const itemMenuToToggle = getParents(event.target, '.menu-item');
  itemMenuToToggle[0].classList.toggle('active');
}

function responsiveItemActive() {
  const menuResposive = q('.ui.navbar.menu.mobile');
  const items = menuResposive.querySelectorAll('.menu-item');
  items.forEach(item => {
    item.addEventListener('click', addActiveMenu);
  });
}

responsiveItemActive();

export default MENU;
