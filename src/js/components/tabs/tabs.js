import { id, all, q } from '../../commons/helpers';

const btnA = id('tabOneButton');
const btnB = id('tabTwoButton');
const pages = all('.page-exito');
const cont = q('.selector-ex');
const tabs = all('.selector-ex-button');

function changeTab(tab) {
  const tabOne = id('tabOne');
  const tabTwo = id('tabTwo');
  if (tabOne && tabTwo) {
    if (tab) {
      tabOne.style.display = 'none';
      tabTwo.style.display = 'block';
      btnA.classList.remove('active');
      btnB.classList.add('active');
    } else {
      tabOne.style.display = 'block';
      tabTwo.style.display = 'none';
      btnA.classList.add('active');
      btnB.classList.remove('active');
    }
  } else if (tabOne) {
    tabOne.style.display = 'block';
    btnA.classList.add('active');
  } else if (tabTwo) {
    tabTwo.style.display = 'block';
    btnB.classList.add('active');
  }
}
if (btnA && btnB) {
  btnA.addEventListener('click', () => {
    changeTab(false);
  });
  btnB.addEventListener('click', () => {
    changeTab(true);
  });
}

function controlShowPages(int = 0) {
  pages.forEach((page) => {
    page.style.display = 'none';
  });
  pages[int].style.display = 'flex';
}

function controlSuccessTabs(int = 0) {
  tabs.forEach((tab) => {
    tab.classList.remove('active');
  });
  tabs[int].classList.add('active');
}

function clickTab(handler) {
  const tabOnClick = handler.target;
  const tab = parseInt(tabOnClick.dataset.tab, 10);
  controlShowPages(tab);
  controlSuccessTabs(tab);
}

function initTabsSuccessPage() {
  if (cont) {
    tabs.forEach((tab) => {
      tab.addEventListener('click', clickTab);
    });
    controlShowPages();
    controlSuccessTabs();
  }
}

changeTab(false);
initTabsSuccessPage();
