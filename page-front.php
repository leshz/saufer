<?php
/*
Template Name: Home Template
Template Post Type: page 
*/?>

<!-- Archivo de cabecera global de Wordpress -->
<?php get_header(); ?>
<!-- Contenido de página de inicio -->
<div id="fullpage">
<?php
  //post type should be books
  //posts_per_page indicates how many posts you want to show
  $type = 'slides';
  $args = array(
    'post_type' => $type,
    'post_status' => 'publish',
    'posts_per_page' => 8,
    'ignore_sticky_posts'=> 0);

  $my_query = null;
  $my_query = new WP_Query($args);
  if( $my_query->have_posts() ) {
    while ($my_query->have_posts()) : $my_query->the_post(); ?>

      <div class="section">
            <div class="background">
              <picture>
                  <source media="(max-width: 500px)" srcset="<?php the_field('img-mobile');?>">
                  <img src="<?php the_field('imagenslide');?>" alt="fondo de pantalla">
              </picture>
            </div>
            <div class="wrap">
             <div class="slide-content">
               <?php
                  add_post_meta( get_the_ID(), 'checked_by_ceo', true, true ); 
               ?>
                <h2><?php the_field('tituloslide') ?></h2>
                <h2><?php the_field('subtituloslide') ?></h2>
                <p><?php the_field('textoslide') ?></p>
                <?php
                  $linktCall = get_field('link_call_to_action');
                
                  if($linktCall != null){
                  ?>
                    <a class="button-slide" href="<?php echo $linktCall['url'] ?>"> <?php echo  $linktCall['title'] ; ?></a>
                <?php 
                  }
                ?>
              </div>
                <!-- <p><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></p> -->
            </div>
      </div>
      
  <?php
    endwhile;
    
  }
  // wp_reset_query();  // Restore global post data stomped by the_post().
  ?>
</div>
<div id="float-menu" class="float-menu-wrap">
  <ul>
    
    <?php 
      if( $my_query->have_posts() ) {
        while ($my_query->have_posts()) : $my_query->the_post();  ?>
          <li class="active" data-menuanchor="<?php echo strtolower(str_replace(" ","_",get_the_title())) ?>" >
            
           
            <a href="#<?php echo strtolower(str_replace(" ","_",get_the_title())) ?>"> 
              <img src="<?php the_field('icon_'); ?> " draggable="false" alt="">
            </a>  
          </li>
    <?php
        endwhile;
    }
      wp_reset_query();  // Restore global post data stomped by the_post().
    ?>
  </ul>
</div>

<!-- Archivo de barra lateral por defecto -->
<?php get_sidebar(); ?>
<!-- Archivo de pié global de Wordpress -->
<?php get_footer(); ?>