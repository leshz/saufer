jQuery(function ($) {

    $('body').on('click', '.misha_upload_image_button', function(e) {
        e.preventDefault();

        var button = $(this);
        var custom_uploader = wp.media({
                title: 'Insert image',
                library: {
                    // uncomment the next line if you want to attach image to the current post
                    // uploadedTo : wp.media.view.settings.post.id, 
                    type: 'image'
                },
                button: {
                    text: 'Use this image' // button label text
                },
                multiple: false // for multiple image selection set to true
            }).on('select', function() { // it also has "open" and "close" events 
                var attachment = custom_uploader.state().get('selection').first().toJSON();
                $(button).removeClass('button').html('<img class="true_pre_image" src="' + attachment.url + '" style="width:100%;max-width:250px;display:block;" />').next().val(attachment.id).next().show();
            })
            .open();
    });

    /*
     * Remove image event
     */
    $('body').on('click', '.misha_remove_image_button', function() {
        $(this).hide().prev().val('').prev().addClass('button').html('Cargar Imagen');
        return false;
    });



    $("#addField").on('click', function(ev) {
        ev.preventDefault();
        var numItem = $(".relevant-item").length

        var newItem = $(".relevant-item").eq(0).clone();
        newItem.find('input').each(function() {
            this.name = this.name.replace('0', numItem)
        });
        newItem.find('input').val("")
        newItem.find('img').remove()
        newItem.find('.misha_remove_image_button').css("display","none")
        newItem.find('.misha_upload_image_button').text('Cargar Imagen').addClass('button')
        
        $('.relevant-container').append(newItem);

    })
    
    $("#addSlideRelevant").on('click', function(ev) {
        ev.preventDefault();
        var numItem = $(".relevant-item-slide").length

        var newItem = $(".relevant-item-slide").eq(0).clone();
        newItem.find('input').each(function() {
            this.name = this.name.replace('0', numItem)
        });
        newItem.find('input').val("")
        newItem.find('img').remove()
        newItem.find('.misha_remove_image_button').css("display","none")
        newItem.find('.misha_upload_image_button').text('Cargar Imagen').addClass('button')
        
        $('.relevant-container-slider').append(newItem);

    })
    
    $(".relevant").on("click", "#delete", function(ev) {
        ev.preventDefault();

        var item = $(this).parents(".relevant-item")
        var ItemsCount = $(".relevant-item").length
            if (ItemsCount === 1 ) {
                alert("no puedes eliminar el unico campo")
            } else {
                item.remove()
            }
        
    })

    $(".sliderProduct").on("click", "#deleteSlide", function(ev) {
        ev.preventDefault();

        var item = $(this).parents(".sliderProduct-item")
        var ItemsCount = $(".sliderProduct-item").length
            if (ItemsCount === 1 ) {
                alert("no puedes eliminar el unico campo")
            } else {
                item.remove()
            }
    })
    $(".relevant-slider").on("click", "#slidedelete", function(ev) {
        ev.preventDefault();

        var item = $(this).parents(".relevant-item-slide")
        var ItemsCount = $("relevant-item-slide").length
            if (ItemsCount === 1 ) {
                alert("no puedes eliminar el unico campo")
            } else {
                item.remove()
            }
    })

    $("#addFieldSlide").on('click', function(ev) {
        ev.preventDefault();
        var numItem = $(".sliderProduct-item").length

        var newItem = $(".sliderProduct-item").eq(0).clone();
        newItem.find('input').each(function() {
            this.name = this.name.replace('0', numItem)
        });
        newItem.find('textarea').each(function() {
            this.name = this.name.replace('0', numItem)
        });
        newItem.find('input').val("")
        newItem.find('img').remove()
        newItem.find('.misha_remove_image_button').css("display","none")
        newItem.find('.misha_upload_image_button').text('Cargar Imagen').addClass('button')
        newItem.find('textarea').val('');
        
        $('.sliderProduct-container').append(newItem);

    })
});