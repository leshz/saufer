<?php 
  (is_page()) ? get_header('content') : get_header();
   wp_head();
   
   // Archivo de cabecera global de Wordpress
   
  if ( have_posts() ) : the_post(); 
    $idPage = get_the_ID();
    $postById = get_post_meta($idPage);
    $firstProduct = explode("|", $postById['product-left'][0]);
    $secondProduct = explode("|", $postById['product-right'][0]);
    endif;
    
?>
    
    <div class="ui container inside-pages">
      <ui class="selector">
        <?php if ($firstProduct[0] != "empty"){ ?>
        <button id="tabOneButton" class='selector-button'><?php echo $firstProduct[1]; ?> </button>
        <?php }
        if ($secondProduct[0] != "empty"){ ?>
        <button id="tabTwoButton" class='selector-button' ><?php echo $secondProduct[1]; ?> </button>
        <?php }?>
      </ui>
    </div>
<!--Fin del bloque WHILE-->


  
  
  <?php
  
  function templateTab($productId , $intTab){
    
    switch ($intTab) {
      case 1:
          $intTab = "tabOne";
          break;
      case 2:
          $intTab = "tabTwo";
          break;
      default:
          $intTab = "";
          break;
  }
  
  $argsForProducts = array(
    'p'=> $productId,
    'post_type' => 'Productos',
    'post_status' => 'publish');

  $product_query = null;
  $product_query = new WP_Query($argsForProducts);
  $productFields = get_fields($productId);
  $getpostProduct = get_post_meta($productId);
  
  $colors = $getpostProduct['colors'][0];
  

  while ($product_query->have_posts()) : $product_query->the_post();
  
    $meta = get_post_meta($productId, 'arrayCaracteristicas', TRUE );
    $meta = json_decode($meta,true);
    
    $slideProduct = get_post_meta($productId, 'dataSlider', TRUE );
    $slideProduct = json_decode($slideProduct,true);

  endwhile;
  ?>
    <div class="ui container pages-products shadow" id="<?php echo $intTab;?>">
    <section>
      <div class="ui grid equal width center aligned padded">
      <div class="column nopadding">
        <div class="banner-image" style="background-image:url(<?php echo get_field('imagenbannersuperior');?>)">
          <div class="banner-image-text">
            <h2> <?php echo get_field('textbannerproducto') ?></h2>
            <?php
              $link = get_field('linkbannerproducto');
              $link_url = $link['url'];
              $link_title = $link['title'];
              $link_target = $link['target'] ? $link['target'] : '_self';
            ?>
            <a class="banner-action <?php echo $colors ;?>" href = "<?php echo esc_url($link_url); ?>" target = "<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title) ?></a>
          </div>
        </div>
      </div>
    </div>
    </section>
    <!---->
    <section>
      <div class="icons-description">
      <?php if (count($meta) > 0 ){ 
        foreach ($meta as $item){?>
          <div class="item">
            <div class="img-cont"><img src="<?php echo wp_get_attachment_image_url($item['imagen'],'Full size')?>"></div>
            <div class="description"><span><?php echo $item['description'];?> </span></div>
          </div>
      <?php } } ?>
    </div>
    </section>
    <!---->
    <?php
    
    $argsForRobots = array(
      'post_type' => 'Automatas',
      'taxonomy' => 'robot-category',
      'term' => $getpostProduct['robot-category'][0],
      'posts_per_page' => 4,
      'nopaging'=> false,
      'orderby'=> 'ID',
      'order'=> 'DESC',);
      
      $robot_query = null;
      $robot_query = new WP_Query($argsForRobots);
      if ($robot_query->have_posts()){
    ?>
    <section>
      <div class="ui grid equal width four center aligned padded gallery stackable">
      <?php while ($robot_query->have_posts()) : $robot_query->the_post(); ?>
      <div class="column nopadding">
        <div class="gallery-item <?php echo $colors ;?>" style="background-image:url(<?php echo get_the_post_thumbnail_url(null , 'medium_large') ?>)">
          <a href="<?php echo get_permalink();?>">
            <div class="gallery-information">
                <div class="float-warp">
                  <h6><?php echo the_title(); ?></h6>
                  <p><?php echo get_field('descripcionrobot') ?></p>
                </div>
            </div>
          </a>
        </div>
      </div>
      <?php endwhile; ?>
      
    </div>
    </section>
    <?php
      }
    ?>
    
    <?php
          $argsForCases = array(
            'post_type' => 'CasosExito',
            'posts_per_page' => 4,
            'nopaging'=> false,
            'orderby'=> 'ID',
            'taxonomy' => 'exito-category',
            'term' => $getpostProduct['exito-category'][0],
            'order'=> 'ASC');
            
            $case_query = null;
            $case_query = new WP_Query($argsForCases);
            $cont = 0;
            
            if ($case_query->have_posts()){
    ?>
    
    
    <section>
      <div class="success-cases">
        <h3 class="<?php echo $colors ;?>">Casos de Éxito</h3>
        <div class="ui grid equal width tree center aligned padded stackable">
          
          
            <?php while ($case_query->have_posts()) : $case_query->the_post();
            // echo the_title();
            if ($cont == 0 || $cont == 1){ ?>
              <div class="column">
                <div class="success-cases-container" style="background-image:url(<?php echo get_the_post_thumbnail_url(null,'medium_large') ?>)" >
                  <div class="description">
                    <p> <?php echo the_title(); ?></p>
                    <p><a href="<?php echo get_permalink(); ?>">Ver todos los casos de exito</a></p>
                  </div>
                </div>
              </div>
              
            <?php
              $cont++;
            }elseif($cont == 2){?>
              <div class="column">
                <div class="row">
                  <div class="success-cases-container middle-top" style="background-image:url(<?php echo get_the_post_thumbnail_url(null,'medium_large') ?>)" >
                    <div class="description">
                      <p> <?php echo the_title(); ?></p>
                      <p><a href="<?php echo get_permalink(); ?>">Ver todos los casos de exito</a></p>
                    </div>
                  </div>
              </div>
            <?php
              $cont++;
            } elseif($cont == 3){ ?>
            
                <div class="row">
                  <div class="success-cases-container middle-botton" style="background-image:url(<?php echo get_the_post_thumbnail_url(null,'medium_large') ?>)">
                    <div class="description">
                      <p> <?php echo the_title(); ?></p>
                      <p><a href="<?php echo get_permalink(); ?>">Ver todos los casos de exito</a></p>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
            <?php endwhile; ?>
        </div>
      </div>
    </section>
    <?php } ?>
    <section>
      <div class="form-contact">
        <div class="form-container">
          <div class="form-item" style="background-image:url(<?php echo wp_get_attachment_image_url($productFields['formulario_de_contacto']['imagen-contacto']['id'],'Full size') ?>)">
            <h2><?php echo $productFields['formulario_de_contacto']['titulo_contacto'];?></h2>
            <h4><?php echo $productFields['formulario_de_contacto']['subtitulo_contacto'] ?></h4>
            <button id="form-display-<?php echo $intTab?>" class="<?php echo $colors;?>">Solicitar Asesoria</button>
          </div>
          <div class="form-item <?php echo $colors;?>">
            <?php 
              if ($productFields['formulario_de_contacto']['formulario_de_contacto'] == '') {
                echo do_shortcode('[contact-form-7 id="105" title="Formulario de asesoría"]');
              }else {
                echo do_shortcode($productFields['formulario_de_contacto']['formulario_de_contacto']);
              }
              ?>
          </div>
        </div>
      </div>
    </section>
    <?php
          $argsForAgenda = array(
            'post_type' => 'AgendaTel',
            'posts_per_page' => 3,
            'taxonomy' => 'agenda-category',
            'term' => $getpostProduct['agenda-category'][0],
            'nopaging'=> false,
            'orderby'=> 'ID',
            'order'=> 'DESC',);
            
            $schedule_query = null;
            $schedule_query = new WP_Query($argsForAgenda);
          
            if ($schedule_query->have_posts()){ ?>
            
    <section>
      <div class="schedule">
        <h3 class="<?php echo $colors ;?>">Agenda Tecnológica</h3>
        <div class="ui grid equal width tree center aligned padded stackable">
          
            
           <?php while ($schedule_query->have_posts()) : $schedule_query->the_post();
          ?>
          <div class="column">
            <div class="schedule-container">
              <div class="image-to-post">
               <a href="<?php echo get_permalink(); ?>">
                  <picture><?php echo get_the_post_thumbnail( '', 'success-thumb' )?></picture> 
                </a>
              </div>
              <div class="content <?php echo $colors ;?>">
                <h3 class="title <?php echo $colors ;?>"><?php echo the_title();?></h3>
                <?php echo the_content('Leer más') ?>
              </div>
            </div>
          </div>
          <?php  endwhile; ?>
        </div>
      </div>
    </section>
    <?php } ?>

  <?php if (!empty($slideProduct)){ ?>
    <section>
      <div class="sld">
      <?php $alt = ($intTab == 'tabTwo') ? "alt" : ''; ?>
        <div class="sld-wrapper<?php echo $alt;?>">
        <?php foreach ($slideProduct as $item){?>
          <div class="sld-item">
            <div class="sld-container">
              <div class="sld-description-container">
                <div class="sld-description-float">
                  <p><?php echo $item['description']?></p>
                </div>
              </div>
              <div class="sld-logo"><img src="<?php echo wp_get_attachment_image_url($item['imagen'],'Full size') ?>"></div>
            </div>
          </div>
        <?php } ?>
        </div>
      </div>
    </section>
  <?php   } ?>
  </div>
  <?php 
    
  } 
  if ($firstProduct[0] != "empty"){
    templateTab($firstProduct[0],1);
  }
  if ($secondProduct[0] != "empty"){ 
    templateTab($secondProduct[0],2);
   }
  ?>
  
  
  
  <div class="iu container full-width">
    <section>
      <div class="googlemaps" id="maps"></div>
    </section>
    <?php
      $key = esc_attr( get_option( 'api_key' ) );
    ?>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo $key; ?>&callback=drawingMap">
    </script>
    <script>
      var center = {
        lat: <?php echo esc_attr( get_option( 'gm_latitude' ) )?>,
        lng: <?php echo esc_attr( get_option( 'gm_longitude' ) )?>
      };
      var options = {
        center: center,
        zoom: 15,
        styles: [{"featureType":"all","elementType":"geometry.fill","stylers":[{"weight":"2.00"}]},{"featureType":"all","elementType":"geometry.stroke","stylers":[{"color":"#9c9c9c"}]},{"featureType":"all","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#eeeeee"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#7b7b7b"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#c8d7d4"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#070707"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]}]
      };
      function drawingMap() {
        var map = new google.maps.Map(document.getElementById('maps'), options);
        var marker = new google.maps.Marker({
        position: center,
        map: map,
        draggable: false,
        animation: google.maps.Animation.DROP,
        title: 'Saufer'
      });
      }
    </script>
  </div>
<!-- Archivo de pié global de Wordpress -->
<?php 

  if(is_page()) {
   get_footer('content');
  }
  else {
   get_footer();
  }
?>