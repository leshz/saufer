# Plantillas para wordpress Saufer MultiSite 

Repositorio en construcción 😅

# Post types:
- Slides Home
- Categorías de Productos
- Caso de Éxito
- Agenda Telefónica
- Robots

	
# Custom Fields:

## SlideCategory (Only Slides Home)
- Título Principal: tituloslide (Text)
- Sub Título: subtituloslide (Text)
- Descripción Slide: textoslide (Text)
- Imagen Slide: imagenslide (Image URL)
- Botón para Slide: link_call_to_action (Link)
- Ícono para el Menú: icon_ (Image URL)

## ProductoCategory (Only Categorías de Productos)
- Texto banner producto: textbannerproducto (Text)
- Imagen Banner Superior: imagenbannersuperior (Image URL)
- Link Banner Producto: linkbannerproducto (Link Array)
- Formulario de Contacto: formulario_de_contacto (Group)

## RobotCategory (Only Robots)
- Subtítulos Agenda: subtitulorobot (Text)
- Link Video Agenda: videorobot (oEmbed)
- Banner Agenda: bannerrobot (Image URL)

## AgendaCategory (Only Agenda Telefónica)
- Subtítulos Agenda: subtituloagenda (Text)
- Link Video Agenda: videoagenda (oEmbed)
- Banner Agenda: banneragenda (Image URL)

## CasoExitoCategory (Only Caso de Éxito)
- Descripción del Caso: descripcioncaso (Text)
- Cliente del Caso: clientecaso (Text)
- Ubicación del Caso: ubicacioncaso (Text)
- Fecha de Instalación: fechacaso (Date Picker)
- Banner del Caso: bannercaso (Image URL)
- Video del Caso: videocaso (oEmbed)
- Link Botón Asesoría: linkasesoria (Link Array)
