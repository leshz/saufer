<?php
/*
Template Name: Contact Template
Template Post Type: page 
*/?>

<?php get_header('agenda');

$productFields = get_fields(get_the_ID());

$items = $productFields['formulario_de_contacto'];
?>

<body>
  <section>
    <div class="ui container section-schedule  contact shadow">
      <article>
        <div class="banner">
        <div class="banner-imagen">
        <picture> 
            <img src="<?php echo $items['imagen_portada'] ?>" alt="Banner para pagina de contactos">
        </picture>
        </div>
  
        </div>
      <h1><?php echo $items['titulo'] ?></h1>
      <div class="form">
        <div class="form-container">
          <div class="form-item">
            
            <?php echo do_shortcode($items['codigo_de_formulario'] ); ?>
          </div>
        </div>
      </div>
      </article>
    </div>
  </section>
  

    
  <div class="iu container full-width">
    <section>
      <div class="googlemaps" id="maps"></div>
    </section>
    <?php
      $key = esc_attr( get_option( 'api_key' ) );
    ?>
    <script defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo $key; ?>&callback=drawingMap">
    </script>
    <script>
      var center = {
        lat: <?php echo esc_attr( get_option( 'gm_latitude' ) )?>,
        lng: <?php echo esc_attr( get_option( 'gm_longitude' ) )?>
      };
      var options = {
        center: center,
        zoom: 15,
        styles: [{"featureType":"all","elementType":"geometry.fill","stylers":[{"weight":"2.00"}]},{"featureType":"all","elementType":"geometry.stroke","stylers":[{"color":"#9c9c9c"}]},{"featureType":"all","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#eeeeee"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#7b7b7b"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#c8d7d4"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#070707"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]}]
      };
      function drawingMap() {
        var map = new google.maps.Map(document.getElementById('maps'), options);
        var marker = new google.maps.Marker({
        position: center,
        map: map,
        draggable: false,
        animation: google.maps.Animation.DROP,
        title: 'Saufer'
      });
      }
    </script>
  </div>

<?php get_footer('agenda'); ?>