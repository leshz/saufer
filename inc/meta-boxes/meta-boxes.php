<?php 

require get_template_directory() . '/inc/meta-boxes/admin/products-admin.php';
require get_template_directory() . '/inc/meta-boxes/admin/relevant-admin.php';
require get_template_directory() . '/inc/meta-boxes/admin/slider-admin.php';
require get_template_directory() . '/inc/meta-boxes/save-post.php';

add_action( 'add_meta_boxes', 'productsInPagesMetaBoxes' ); 
add_action( 'add_meta_boxes', 'sliderProducts');
add_action( 'add_meta_boxes', 'relevantCaraxcteristicsinProduct');//
add_action( 'add_meta_boxes', 'relevantCaracteristicsinCases');//
add_action( 'add_meta_boxes', 'relevantSlideinCases');//
add_action( 'add_meta_boxes', 'productsCategoriesInProductsMetaBoxes');
add_action( 'add_meta_boxes', 'productsCategoriesInAgendaMetaBoxes');
add_action( 'add_meta_boxes', 'productsCategoriesInExito');
add_action( 'add_meta_boxes', 'colorsinProducts');


function productsInPagesMetaBoxes()  { 
	add_meta_box( 
						'meta-box-id',
						'Productos',
						'productsInPageCallback',
						'page',
						'normal',
						'high'
					);  
}
function colorsinProducts()  { 
	add_meta_box( 
					'meta-box-color-id',
					'Seleccionar Color',
					'colors_in_products',
					'productos',
					'normal',
					'low'
				);  
}

function sliderProducts(){  
	add_meta_box( 
						'sliderProducts',
						'slider de productos', 
						'sliderAdmin', 
						'productos', 
						'normal', 
						'low' 
					);  
}
//
function relevantCaraxcteristicsinProduct(){  
	add_meta_box( 
					'relevantItems', 
					'Características de producto', 
					'relevantViewAdmin',
					'productos', 
					'normal', 
					'low' 
				);  
}

function relevantCaracteristicsinCases(){  
	add_meta_box( 
					'relevantItemsCases', 
					'Características de Casos de Exito', 
					'relevantViewAdminCases',
					'CasosExito', 
					'normal', 
					'low' 
				);  
}

function relevantSlideinCases(){  
	add_meta_box( 
					'relevantSliderCases', 
					'Imagenes Slider de Casos de Exito', 
					'relevantViewAdminSliderCases',
					'CasosExito', 
					'normal', 
					'low' 
				);  
}


function productsCategoriesInProductsMetaBoxes() { 
		add_meta_box( 
						'categories-meta-box',
						'Categoria de Robot',
						'categoriesInProductCallback',
						'productos',
						'normal',
						'high'
					);  
}

function productsCategoriesInAgendaMetaBoxes(){
		add_meta_box( 
						'agenda-meta-box',
						'Categoria de Agendas',
						'agendasInProductCallback',
						'productos',
						'normal',
						'high'
					); 
	
}

function productsCategoriesInExito()  { 
		add_meta_box( 
						'Exito-meta-box',
						'Categoria Casos de Exito',
						'exitoInProducts',
						'productos',
						'normal',
						'high'
					);  
}
?>