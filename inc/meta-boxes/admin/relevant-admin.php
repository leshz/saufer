<?php
function relevantViewAdmin($post){

  wp_enqueue_script( 'myuploadscript', get_stylesheet_directory_uri() . '/admin/admin.js', array('jquery'), null, false );
  wp_enqueue_style( 'myuploadstyle', get_stylesheet_directory_uri() . '/admin/admin.css', array(), null, 'all' );

  $values = get_post_custom($post->ID);
  $data = (isset($values['arrayCaracteristicas'][0])) ? json_decode($values['arrayCaracteristicas'][0],true) : null;

?>
<div class="relevant">
  <div class="relevant-container">
<?php 
      if($data != null){
        foreach ($data as $key => $item){
          
          $url = wp_get_attachment_image_url($item['imagen'],'Medium size');
?>
          <div class="relevant-item">
            <div class="relevant-item-image">
            <button href="#" class="misha_upload_image_button"><img class="true_pre_image" src="<?php echo $url ;?>" style="width:100%;max-width:250px;display:block;"></button>
              <input type="hidden" name="rel[<?php echo $key; ?>][imagen]" id="" value="<?php echo $item['imagen'];?>">
              <a href="#" class="misha_remove_image_button" style="">Remover imagen</a>
            </div>
            <div class="relevant-item-text">
              <label for="text">Descripción</label>
              <input type="text" id="text" name="rel[<?php echo $key;?>][description]" maxlength="60" value="<?php echo $item['description'];?>" class="input-text">
            </div>
            <div class="relevant-item-buttons">
              <button id="delete">x</button>
            </div>
          </div>
          
          
<?php 
}      
} 
  else { 
?>
        <div class="relevant-item">
          <div class="relevant-item-image">
            <button href="#" class="misha_upload_image_button button">Cargar Imagen</button>
            <input type="hidden" name="rel[0][imagen]" id="" value="" />
            <a href="#" class="misha_remove_image_button" style="display:none;">Remover imagen</a>
          </div>
          <div class="relevant-item-text">
            <label for="text">Descripción</label>
            <input type="text" id="text" name="rel[0][description]" maxlength="60" value="" class="input-text">
          </div>
          <div class="relevant-item-buttons">
            <button id="delete">x</button>
          </div>
        </div>
<?php 
      }
?>
    
  </div>
    <div class="relevant-button-plus">
        <button class="button" id="addField">Agregar Item</button>
  </div>
</div>

<?php 
}

function relevantViewAdminCases($post){

wp_enqueue_script( 'myuploadscript', get_stylesheet_directory_uri() . '/admin/admin.js', array('jquery'), null, false );
wp_enqueue_style( 'myuploadstyle', get_stylesheet_directory_uri() . '/admin/admin.css', array(), null, 'all' );

$values = get_post_custom($post->ID);
$data = (isset($values['arrayCaracteristicasExito'][0])) ? json_decode($values['arrayCaracteristicasExito'][0],true) : null;

?>
<div class="relevant">
  <div class="relevant-container">
<?php 
      if($data != null){
        foreach ($data as $key => $item){
          
          $url = wp_get_attachment_image_url($item['imagen'],'Medium size');
?>
          <div class="relevant-item">
            <div class="relevant-item-image">
            <button href="#" class="misha_upload_image_button"><img class="true_pre_image" src="<?php echo $url ;?>" style="width:100%;max-width:250px;display:block;"></button>
              <input type="hidden" name="exito-caract[<?php echo $key; ?>][imagen]" id="" value="<?php echo $item['imagen'];?>">
              <a href="#" class="misha_remove_image_button" style="">Remover imagen</a>
            </div>
            <div class="relevant-item-text">
              <label for="text">Descripción</label>
              <input type="text" id="text" name="exito-caract[<?php echo $key;?>][description]" maxlength="60" value="<?php echo $item['description'];?>" class="input-text">
            </div>
            <div class="relevant-item-buttons">
              <button id="delete">x</button>
            </div>
          </div>
          
          
<?php 
          
        }
        
      } 
      else { 
?>
        <div class="relevant-item">
          <div class="relevant-item-image">
            <button href="#" class="misha_upload_image_button button">Cargar Imagen</button>
            <input type="hidden" name="exito-caract[0][imagen]" id="" value="" />
            <a href="#" class="misha_remove_image_button" style="display:none;">Remover imagen</a>
          </div>
          <div class="relevant-item-text">
            <label for="text">Descripción</label>
            <input type="text" id="text" name="exito-caract[0][description]" maxlength="60" value="" class="input-text">
          </div>
          <div class="relevant-item-buttons">
            <button id="delete">x</button>
          </div>
        </div>
<?php 
      }
?>
    
  </div>
    <div class="relevant-button-plus">
        <button class="button" id="addField">Agregar Item</button>
  </div>
</div>

<?php 
}
?>
<?php

function relevantViewAdminSliderCases($post){

wp_enqueue_script( 'myuploadscript', get_stylesheet_directory_uri() . '/admin/admin.js', array('jquery'), null, false );
wp_enqueue_style( 'myuploadstyle', get_stylesheet_directory_uri() . '/admin/admin.css', array(), null, 'all' );

$values = get_post_custom($post->ID);
$data = (isset($values['arraySlideCases'][0])) ? json_decode($values['arraySlideCases'][0],true) : null;

?>
<div class="relevant-slider">
  <div class="relevant-container-slider">
<?php 
      if($data != null){
        foreach ($data as $key => $item){
          
          $url = wp_get_attachment_image_url($item['imagen'],'Medium size');
?>
          <div class="relevant-item-slide">
            <div class="relevant-item-image">
            <button href="#" class="misha_upload_image_button"><img class="true_pre_image" src="<?php echo $url ;?>" style="width:100%;max-width:250px;display:block;"></button>
              <input type="hidden" name="relslide[<?php echo $key; ?>][imagen]" id="" value="<?php echo $item['imagen'];?>">
              <a href="#" class="misha_remove_image_button" style="">Remover imagen</a>
            </div>
            <div class="relevant-item-buttons">
              <button id="slidedelete">x</button>
            </div>
          </div>
          
          
<?php 
          
        }
        
      } 
      else { 
?>
          <div class="relevant-item-image">
            <button href="#" class="misha_upload_image_button button">Cargar Imagen</button>
            <input type="hidden" name="relslide[0][imagen]" id="" value="" />
            <a href="#" class="misha_remove_image_button" style="display:none;">Remover imagen</a>
          </div>
          <div class="relevant-item-buttons">
            <button id="deleteSlide">x</button>
          </div>
        </div>
      <?php 
      }
    ?>
    
  </div>
    <div class="relevant-button-plus">
        <button class="button" type="button" id="addSlideRelevant">Agregar Item</button>
  </div>
</div>

<?php 
}
?>