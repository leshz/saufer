<?php 

function sliderAdmin ($post){
	
	wp_enqueue_script( 'myuploadscript', get_stylesheet_directory_uri() . '/admin/admin.js', array('jquery'), null, false );
  wp_enqueue_style( 'myuploadstyle', get_stylesheet_directory_uri() . '/admin/admin.css', array(), null, 'all' );

  $values = get_post_custom($post->ID);
  $data = (isset($values['dataSlider'][0])) ? json_decode($values['dataSlider'][0],true) : null;
?> 
<div class="sliderProduct">
  <div class="sliderProduct-container">

  <?php 
    if($data != null){
        foreach ($data as $key => $item){
          $url = wp_get_attachment_image_url($item['imagen'],'Medium size');
          ?>
          <div class="sliderProduct-item">
            <div class="sliderProduct-item-image">
            <button href="#" class="misha_upload_image_button"><img class="true_pre_image" src="<?php echo $url ;?>" style="width:100%;max-width:250px;display:block;"></button>
              <input type="hidden" name="sld[<?php echo $key; ?>][imagen]" id="" value="<?php echo $item['imagen'];?>">
              <a href="#" class="misha_remove_image_button" style="">Remover imagen</a>
            </div>
            <div class="sliderProduct-item-text">
              <label for="text">Descripción</label>
              <textarea type="text" id="text" name="sld[<?php echo $key;?>][description]"  class="input-text"><?php echo $item['description'];?></textarea>
            </div>
          <div class="sliderProduct-item-buttons">
            <button id="deleteSlide">x</button>
          </div>
        </div>
      <?php
        }
    }
    else {?>
      <div class="sliderProduct-item">
        <div class="sliderProduct-item-image">
          <button type="button" href="#" class="misha_upload_image_button button">Cargar Imagen</button>
          <input type="hidden" name="sld[0][imagen]" id="" value="" />
          <a href="#" class="misha_remove_image_button" style="display:none;">Remover imagen</a>
        </div>
        <div class="sliderProduct-item-text">
          <label for="text">Descripción</label>
          <textarea type="text" id="text" name="sld[0][description]" maxlength="60" value="" class="input-text"></textarea>
        </div>
        <div class="sliderProduct-item-buttons">
          <button id="deleteSlide" type="button">x</button>
        </div>
      </div>
    <?php 
    }
?>
  </div>
  <div class="sliderProduct-button-plus">
    <button type="button" class="button" id="addFieldSlide">Agregar Item</button>
  </div>
</div>

<?php
}
