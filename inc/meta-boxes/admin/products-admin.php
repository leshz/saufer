<?php
function productsInPageCallback($post){

  wp_enqueue_style( 'myuploadstyle', get_stylesheet_directory_uri() . '/admin/admin.css', array(), null, 'all' );

  $values = get_post_custom($post->ID);
  $selected_left = isset( $values['product-left'] ) ? esc_attr( $values['product-left'][0] ) : '';
  $selected_right = isset( $values['product-right'] ) ? esc_attr( $values['product-right'][0] ) : '';

  $idByAdminLeft = explode('|',$selected_left);
  $idByAdminRight = explode('|',$selected_right);
  
  $query = array(
    'post_type' => 'productos',
    'post_status' => 'publish',
    'posts_per_page' => -1
  );
  $products = new WP_Query( $query );  
  $posts = $products->posts;

?>
<div class="selector-page">
  <div class="selector-page-item">
    <label for="product-left">Producto Izquierda</label>
    <select name="product-left" id="product-left">
      <option value="empty" > Sin Producto </option>
      <?php
        foreach ($posts as $post) {
      ?>
          <option value="<?php echo $post->ID; echo "|"; echo $post->post_title; ?>" <?php selected( $idByAdminLeft[0] , $post->ID ); ?> > <?php echo $post->post_title ?></option>";
      <?php
        }
      ?>
    </select>
  </div>

  <div class="selector-page-item">
    <label for="product-right">Producto Derecha</label>
    <select name="product-right" id="product-right">
        <option value="empty" > Sin Producto </option>
        <?php
        foreach ($posts as $post) {
        ?>
          <option value="<?php echo $post->ID;echo "|"; echo $post->post_title; ?>" <?php selected( $idByAdminRight[0] , $post->ID ); ?> > <?php echo $post->post_title ?></option>";
      <?php 
        }
      ?>
    </select>
  </div>
</div>
<?php
}


function categoriesInProductCallback($post){
  
  wp_enqueue_style( 'myuploadstyle', get_stylesheet_directory_uri() . '/admin/admin.css', array(), null, 'all' );
  
  
 $values = get_post_custom($post->ID);
 $robot_category = $values['robot-category'][0];
 
  $categories = get_categories( array(
        'title_li' => '',
        'style'    => 'none',
        'echo'     => false,
        'taxonomy' => 'robot-category') 
        );
  ?>
  <div class="selector-page">
      <div class="selector-page-item">
        <select name="mrrobot">
          <?php
          foreach ($categories as $category){
          ?>
            <option value="<?php echo $category->slug; ?>"  <?php selected( $robot_category , $category->slug)?> > <?php echo $category->name; ?></option>
          <?php
            }
          ?>
      </select>
    </div>
  </div>

<?php 
  
}

function agendasInProductCallback($post){
  
  wp_enqueue_style( 'myuploadstyle', get_stylesheet_directory_uri() . '/admin/admin.css', array(), null, 'all' );
  
  $values = get_post_custom($post->ID);
  $agenda_category = $values['agenda-category'][0];
  
  $categories = get_categories( array(
          'title_li' => '',
          'style'    => 'none',
          'echo'     => false,
          'taxonomy' => 'agenda-category') 
          );
          
  ?>
  <div class="selector-page">
      <div class="selector-page-item">
        <select name="agenda-category">
          <?php
          foreach ($categories as $category){
          ?>
            <option value="<?php echo $category->slug; ?>"  <?php selected( $agenda_category , $category->slug)?> > <?php echo $category->name; ?></option>
          <?php
            }
          ?>
      </select>
    </div>
  </div>

<?php
  
  
}


function exitoInProducts($post){
  
  wp_enqueue_style( 'myuploadstyle', get_stylesheet_directory_uri() . '/admin/admin.css', array(), null, 'all' );
  $values = get_post_custom($post->ID);

  $exito_category = $values['exito-category'][0];

  $categories = get_categories( array(
      'title_li' => '',
      'style'    => 'none',
      'echo'     => false,
      'taxonomy' => 'exito-category') 
  );
  
  
  ?>
    <div class="selector-page">
      <div class="selector-page-item">
        <select name="exito-category">
          <?php
          foreach ($categories as $category){
          ?>
            <option value="<?php echo $category->slug; ?>"  <?php selected( $exito_category , $category->slug)?> > <?php echo $category->name; ?></option>
          <?php
            }
          ?>
      </select>
    </div>
  </div>
<?php
}

function colors_in_products($post){
  wp_enqueue_style( 'myuploadstyle', get_stylesheet_directory_uri() . '/admin/admin.css', array(), null, 'all' );
  $values = get_post_custom($post->ID);
  
  $colorsProduct = $values['colors'][0];
  
?>
    
   <div class="selector-page">
     <div class="selector-page-item">
       <select name="color">
          <option value="" <?php selected("", $colorsProduct)?> >Default</option>
          <option value="red" <?php selected( "red" , $colorsProduct)?> >Rojo</option>
          <option value="orange" <?php selected("orange"  , $colorsProduct)?> >Naranja</option>
          <option value="green" <?php selected( "green" , $colorsProduct)?> >Verde</option>
          <option value="blue" <?php selected(  "blue", $colorsProduct)?> >Azul</option>
          <option value="green-dark" <?php selected( "green-dark" , $colorsProduct)?> >Verde Oscuro</option>
       </select>
     </div>
   </div>
<?php
}

?>