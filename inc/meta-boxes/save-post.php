<?php

add_action( 'save_post', 'productsInPagesMetaBoxes_save' );

function productsInPagesMetaBoxes_save( $post_id )  {  
    
    // Bail if we're doing an auto save  
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return; 
  
    // if our current user can't edit this post, bail  
    if( !current_user_can( 'edit_post' ) ) return;  

    if( isset( $_POST['product-left'] ) )  
      update_post_meta( $post_id, 'product-left', wp_kses( $_POST['product-left'], $allowed ) );  
    if( isset( $_POST['product-right'] ) )  
      update_post_meta( $post_id, 'product-right', wp_kses( $_POST['product-right'], $allowed ) );

    if( isset( $_POST['rel'] ) ){
      $data = $_POST['rel'] ;
      foreach ( $data as $key => $item ){
        if($item['imagen'] == '' || $item['description'] == '' ){
          unset($data[$key]);
        }
      }
      $dataToSave = json_encode($data,JSON_UNESCAPED_UNICODE);
      update_post_meta( $post_id, 'arrayCaracteristicas', $dataToSave );
    }
    if( isset( $_POST['exito-caract'] ) ){
      $data = $_POST['exito-caract'] ;
      foreach ( $data as $key => $item ){
        if($item['imagen'] == '' || $item['description'] == '' ){
          unset($data[$key]);
        }
      }
      $dataToSave = json_encode($data,JSON_UNESCAPED_UNICODE);
      update_post_meta( $post_id, 'arrayCaracteristicasExito', $dataToSave );
    }

    if( isset( $_POST['relslide'] ) ){
      $data = $_POST['relslide'] ;
      foreach ( $data as $key => $item ){
        if($item['imagen'] == ''  ){
          unset($data[$key]);
        }
      }
      $dataToSave = json_encode($data,JSON_UNESCAPED_UNICODE);
      update_post_meta( $post_id, 'arraySlideCases', $dataToSave );
    }
    if( isset( $_POST['sld'] ) ){
      $data = $_POST['sld'] ;
      foreach ( $data as $key => $item ){
        if($item['imagen'] == '' || $item['description'] == '' ){
          unset($data[$key]);
        }
      }
      $dataToSave = json_encode($data,JSON_UNESCAPED_UNICODE);
      update_post_meta( $post_id, 'dataSlider', $dataToSave );
    }
    
    if(isset($_POST['mrrobot'])){
      
      update_post_meta( $post_id, 'robot-category', wp_kses( $_POST['mrrobot'], $allowed ) ); 
      
    }
    
    if(isset($_POST['exito-category'])){
      
      update_post_meta( $post_id, 'exito-category', wp_kses( $_POST['exito-category'], $allowed ) ); 
      
    }
    
    if(isset($_POST['agenda-category'])){
      
      update_post_meta( $post_id, 'agenda-category', wp_kses( $_POST['agenda-category'], $allowed ) ); 
      
    }
   if(isset($_POST['color'])){
      
      update_post_meta( $post_id, 'colors', wp_kses( $_POST['color'], $allowed ) ); 
      
    }
    
}

?>