<?php
function saufer_add_admin_page() {
	
	//Generate Sunset Admin Page
	add_menu_page( 'Saufer Theme', 'Saufer Theme', 'manage_options', 'submenu_saufer', 'saufer_theme_create_page', get_template_directory_uri() . '/static/images/logo.png', 110 );
	
	//Generate Sunset Admin Sub Pages
	add_submenu_page( 'submenu_saufer', 'Saufer Theme Options', 'General', 'manage_options', 'submenu_saufer', 'saufer_theme_create_page' );
	add_submenu_page( 'submenu_saufer', 'Saufer CSS Options', 'Custom CSS', 'manage_options', 'submenu_saufer_css', 'sunset_theme_settings_page');

}
add_action( 'admin_menu', 'saufer_add_admin_page' );

//Activate custom settings
add_action( 'admin_init', 'sunset_custom_settings' );

function sunset_custom_settings() {
	register_setting( 'saufer-settings-group', 'gm_longitude' );
	register_setting( 'saufer-settings-group', 'gm_latitude' );
	register_setting( 'saufer-settings-group', 'api_key' );
	register_setting( 'saufer-settings-group', 'link_facebook' );
	register_setting( 'saufer-settings-group', 'link_instagram' );
	register_setting( 'saufer-settings-group', 'link_linkedin' );
	register_setting( 'saufer-settings-group', 'link_twitter' );
	register_setting( 'saufer-settings-group', 'info_footer_one' );
	register_setting( 'saufer-settings-group', 'info_footer_two' );
	register_setting( 'saufer-settings-group', 'info_footer_sucursales' );
	register_setting( 'saufer-settings-group', 'info_footer_sucursales_2' );
	
	
	add_settings_section( 'sunset-sidebar-options', 'Información de Google Maps', 'saufer_sidebar_options', 'submenu_saufer');
	add_settings_section( 'sunset-api-options', 'API Key', 'saufer_api_options', 'submenu_saufer');
	add_settings_section( 'footer-section', 'Informacion Footer', 'saufer_footer_section', 'submenu_saufer');
	add_settings_section( 'sunset-social-media', 'Redes Sociales', 'saufer_social_media', 'submenu_saufer');
	add_settings_section( 'sunset-sucursales', 'Informacion Sucursales', 'saufer_sucursales', 'submenu_saufer');
	

	add_settings_field( 'foo-one', 'Información Bloque 1', 'sunset_sidebar_footer_one', 'submenu_saufer', 'footer-section');
	add_settings_field( 'foo-two', 'Información Bloque 2', 'sunset_sidebar_footer_two', 'submenu_saufer', 'footer-section');
	
	add_settings_field( 'sidebar-longitude', 'Longitud', 'sunset_sidebar_longitude', 'submenu_saufer', 'sunset-sidebar-options');
	add_settings_field( 'sidebar-latitude', 'Latitud', 'sunset_sidebar_latitude', 'submenu_saufer', 'sunset-sidebar-options');
	add_settings_field( 'sidebar-api', 'API Key', 'sunset_sidebar_api', 'submenu_saufer', 'sunset-api-options');
	add_settings_field( 'sm-facebook', 'Facebook', 'sunset_sidebar_fb', 'submenu_saufer', 'sunset-social-media');
	add_settings_field( 'sm-instagram', 'Instagram', 'sunset_sidebar_ig', 'submenu_saufer', 'sunset-social-media');
	add_settings_field( 'sm-linkedin', 'Linkedn', 'sunset_sidebar_ln', 'submenu_saufer', 'sunset-social-media');
	add_settings_field( 'sm-twitter', 'Twitter', 'sunset_sidebar_tw', 'submenu_saufer', 'sunset-social-media');
	
	add_settings_field( 'sucursales-admin', '', 'sunset_sidebar_footer_sucursales', 'submenu_saufer', 'sunset-sucursales');
	add_settings_field( 'sucursales-admin_2', '', 'sunset_sidebar_footer_sucursales_2', 'submenu_saufer', 'sunset-sucursales');
	
	
	
}

function saufer_sidebar_options() {
	echo 'Ingresa los datos de longitud y latitud para el mapa del footer';
}

function saufer_api_options() {
	echo 'Ingresa el API Key Para Google Maps ';
}

function saufer_social_media() {
	echo 'Ingresa los links de redes sociales.';
}

function saufer_sucursales() {?>
	<p> Informacion sobre sucursales en Footer</p>
<?php 
	
}


function saufer_footer_section() {
	echo 'Información del Footer en la pagina principal';
}

function sunset_sidebar_longitude() {
	$longitude = esc_attr( get_option( 'gm_longitude' ) );
	echo '<input type="text" name="gm_longitude" value="'.$longitude.'" placeholder="Longitud" />';
}

function sunset_sidebar_latitude() {
	$latitude = esc_attr( get_option( 'gm_latitude' ) );
	echo '<input type="text" name="gm_latitude" value="'.$latitude.'" placeholder="Latitud" />';
}

function sunset_sidebar_api() {
	$apikey = esc_attr( get_option( 'api_key' ) );
	echo '<input type="text" name="api_key" value="'.$apikey.'" placeholder="API Key" />';
}

function sunset_sidebar_fb() {
	$fb = esc_attr( get_option( 'link_facebook' ) );
	echo '<input type="text" name="link_facebook" value="'.$fb.'" placeholder="Facebook" />';
}

function sunset_sidebar_ig() {
	$ig = esc_attr( get_option( 'link_instagram' ) );
	echo '<input type="text" name="link_instagram" value="'.$ig.'" placeholder="Instagram" />';
}

function sunset_sidebar_ln() {
	$ln = esc_attr( get_option( 'link_linkedin' ) );
	echo '<input type="text" name="link_linkedin" value="'.$ln.'" placeholder="Linkedin" />';
}

function sunset_sidebar_tw() {
	$tw = esc_attr( get_option( 'link_twitter' ) );
	echo '<input type="text" name="link_twitter" value="'.$tw.'" placeholder="Twitter" />';
}

function sunset_sidebar_footer_one() {
	$foo_one = get_option( 'info_footer_one' );
	
	$options_editor = array (
		'media_buttons' => false ,
		'quicktags' => false,
		'textarea_rows' => 5,
		'teeny'=> true,
		'textarea_name' => 'info_footer_one'
		);
	

  echo wp_editor( $foo_one, 'foo_One', $options_editor  );

}

function sunset_sidebar_footer_two() {
	$foo_two = get_option( 'info_footer_two' );
	
	$options_editor = array (
		'media_buttons' => false ,
		'quicktags' => false,
		'textarea_rows' => 5,
		'teeny'=> true,
		'textarea_name' => 'info_footer_two'
		);
	

  echo wp_editor( $foo_two, 'foo_two', $options_editor  );

}


function sunset_sidebar_footer_sucursales() {
	$foo_sucursales = get_option( 'info_footer_sucursales' );
	
	$options_editor = array (
		'media_buttons' => false ,
		'quicktags' => false,
		'textarea_rows' => 5,
		'teeny'=> true,
		'textarea_name' => 'info_footer_sucursales'
		);
	
  echo wp_editor( $foo_sucursales, 'info_footer_sucursales', $options_editor );

}

function sunset_sidebar_footer_sucursales_2() {
	$foo_sucursales = get_option( 'info_footer_sucursales_2' );
	
	$options_editor = array (
		'media_buttons' => false ,
		'quicktags' => false,
		'textarea_rows' => 5,
		'teeny'=> true,
		'textarea_name' => 'info_footer_sucursales_2'
		);
	
  echo wp_editor( $foo_sucursales, 'info_footer_sucursales_2', $options_editor );

}


function saufer_theme_create_page() {
	require get_template_directory().'/inc/template/admin_theme.tpl.php'; 
}

function sunset_theme_settings_page() {
	
	echo '<h1>Saufer Custom CSS</h1>';
	
}

function customcss() {
    ?>
    <style>
        #adminmenu .wp-menu-image img{
        	    padding: 7px 0 0 0 !important;
        }
    </style>
    <?php
}

add_action( 'admin_head', 'customcss' );


?>