<?php 

add_action('init', 'mis_menus');

function mis_menus() {
  register_nav_menus(
    array(
      'navegation' => __( 'Menú de navegación' ),
    )
  );
}

?>