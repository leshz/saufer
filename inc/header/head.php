<?php
function headByRequest($typeOfCSS = ''){
    
$theme = wp_get_theme();
$ver = $theme->get( 'Version' );


    $url = get_template_directory_uri();
    switch ($typeOfCSS) {
        case 'product':
            $css = "<link href='".$url."/css/products.css?ver=".$ver."' rel='stylesheet'>";
            break;
            
        case 'agenda':
            $css = "<link href='".$url."/css/agenda.css?ver=".$ver."' rel='stylesheet'>";
            break;
            
        case 'exito':
            $css = "<link href='".$url."/css/exito.css?ver=".$ver."' rel='stylesheet'>";
            break;
        
        default:
            $css = "<link href='".$url."/css/index.css?ver=".$ver."' rel='stylesheet'>";
            break;
    }

    return $css;     
}
?>

