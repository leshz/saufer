<!DOCTYPE html>
  <html lang="<?php bloginfo('language'); ?>">
      <head>
          <meta charset="<?php bloginfo('charset'); ?>">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <meta http-equiv="X-UA-Compatible" content="ie=edge">
          <title> <?php bloginfo(‘name’); ?> <?php wp_title(' | ', true , ‘right’); ?></title>
          <script src="https://cdn.polyfill.io/v2/polyfill.js"></script>
          <?php echo $css ?>
              <!-- Smartsupp Live Chat script -->
        <script type="text/javascript">
            var _smartsupp = _smartsupp || {};
            _smartsupp.key = 'ef8d906ead26313f53c8eeb66789741598fefd74';
            window.smartsupp||(function(d) {
            var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
            s=d.getElementsByTagName('script')[0];c=d.createElement('script');
            c.type='text/javascript';c.charset='utf-8';c.async=true;
            c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
            })(document);
        </script>
        <?php wp_head(); ?>
      </head> 
  <body>