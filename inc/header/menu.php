<header>
    <nav id="menutop">
    <div class="ui navbar menu desktop">
        <div class="menu-container"> 
            <?php wp_nav_menu( array( 'theme_location' => 'navegation',
                                    'menu_class' => 'thirteen wide column',
                                    'container' => false
                                ) ); ?>
            <div class="one column">
                <div class="menu-icon">
                    <a href="<?php echo get_home_url(); ?>">
                        <img src="<?php echo get_template_directory_uri();?>/static/images/LogoWhite.png" alt="Saufer logo">
                            <!--<img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MCA1MS4zMyI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiNmZmZ9PC9zdHlsZT48L2RlZnM+PGcgaWQ9Ik1lbsO6Ij48cGF0aCBjbGFzcz0iY2xzLTEiIGQ9Ik00NS42OCAyMi45M3Y2LjI5Yy0uMDguMzItLjE3LjY0LS4yNSAxYTIwLjUyIDIwLjUyIDAgMCAxLTEwLjY3IDEzLjkgMjEuOTMgMjEuOTMgMCAwIDEtNi44OSAyLjE5SDIzbC0uNjktLjE1YTIwLjM1IDIwLjM1IDAgMCAxLTE0LjUtMTAgMjEuNDggMjEuNDggMCAwIDEtMi42LTcuNjZ2LTVjLjA4LS4zOC4xOC0uNzYuMjYtMS4xNGEyMC4yMSAyMC4yMSAwIDAgMSA5LjctMTMuNzQgMjEuNjEgMjEuNjEgMCAwIDEgOC0yLjc1aDQuNjhjLjIgMCAuNDEuMS42MS4xM2EyMC4yNSAyMC4yNSAwIDAgMSAxNC43MiAxMCAyMi4wNyAyMi4wNyAwIDAgMSAyLjUgNi45M3pNMjUuNDggOC4zNGExNy44MSAxNy44MSAwIDEgMCAwIDM1LjYyIDE3LjgxIDE3LjgxIDAgMSAwIDAtMzUuNjJ6Ii8+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNMjUuNTYgNDIuMjJBMTYuMjcgMTYuMjcgMCAwIDEgMTQgMzcuNGExLjY0IDEuNjQgMCAwIDEtLjQyLTIgMTMuOTQgMTMuOTQgMCAwIDEgNC45My02LjU5IDEuMSAxLjEgMCAwIDEgMS41MSAwIDguODcgOC44NyAwIDAgMCAxMC43OC4xIDEuMzYgMS4zNiAwIDAgMSAxLjg5IDAgMTQuNTUgMTQuNTUgMCAwIDEgNC44OCA3IDEgMSAwIDAgMS0uMzIgMS4yIDE2LjMzIDE2LjMzIDAgMCAxLTExLjY5IDUuMTF6Ii8+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNMTcuOTEgMjEuMzRhNy41NyA3LjU3IDAgMCAxIDcuNi03LjYxIDcuNjYgNy42NiAwIDAgMSAwIDE1LjMyIDcuNTkgNy41OSAwIDAgMS03LjYtNy43MXoiLz48L2c+PC9zdmc+">-->
                            </a>
                </div>
            </div>

        </div>
    </div>
        <div class="ui navbar menu mobile shadow">
            <div class="menu-container">
                <div class="row">
                    <div class="top-container">
                        <div class="top-item">
                            <div id="burger">
                                <div class="toggle">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <!--<div class="top-item">-->
                        <!--	<div class="search"> </div>-->
                        <!--</div>-->
                        <div class="top-item">
                            <div class="menu-icon">
                                <a href="<?php echo get_home_url(); ?>">
                                    <img src="<?php echo get_template_directory_uri();?>/static/images/HOME-02.svg"></div>
                                </a>
                        </div>
                    </div>
                </div>
                <div class="row nopadding">
            <?php wp_nav_menu( 
                array( 
                    'theme_location' => 'navegation',
                    'menu_class' => 'ten wide column center animation-mobile',
                    'container' => false
            ) 
            ); ?>
                </div>
            </div>
        </div>
    </nav>
</header>


