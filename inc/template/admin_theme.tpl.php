<h1>Saufer Theme Options</h1>
<?php settings_errors(); ?>
<form action="options.php" method="post">
    <?php settings_fields('saufer-settings-group'); ?>
    <?php do_settings_sections('submenu_saufer');?>
    <?php submit_button(); ?>
</form>