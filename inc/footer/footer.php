<?php
function footerByRequest($typeOfCSS = ''){
    
$theme = wp_get_theme();
$ver = $theme->get( 'Version' );

    $url = get_template_directory_uri();
    switch ($typeOfCSS) {
        case 'product':
            $script = "<script async src='".$url."/js/products.js?ver=".$ver."'></script>";
            break;
            
        case 'agenda':
            $script = "<script async src='".$url."/js/agenda.js?ver=".$ver."'></script>";
            break;
            
        case 'exito':
            $script = "<script async src='".$url."/js/exito.js?ver=".$ver."'></script>";
            break;
        
        default:
            $script = "<script async src='".$url."/js/index.js?ver=".$ver."'></script>";
            break;
    }

    return $script;     
}
?>
