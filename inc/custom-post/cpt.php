<?php
add_action('init', 'crear_un_cpt');
add_action( 'init', 'exito_custom_taxonomy' );
add_action( 'init', 'robot_custom_taxonomy' );
add_action( 'init', 'agenda_custom_taxonomy' );

function crear_un_cpt() {
	
     $argsSlide = array(
     'public' => true,
     'label' => 'Slides Home',
     'menu_icon'  => 'dashicons-slides',
     'supports' => array('title'),
     'show_in_nav_menus' => false,
   	 'publicly_queryable' => false,
     );
     
     $argsRobot = array(
        'public' => true,
        'label' => 'Robots',
        'menu_icon'  => 'dashicons-smiley',
        'taxonomies'=> array( 'robot-category' ),
        'supports'=> array('title', 'editor', 'thumbnail'),
        'rewrite' => array('slug' => 'automatas'),
        'show_in_nav_menus' => true,
       );
     
     $argsProducto = array(
        'public' => true,
        'label' => 'Categorias de Productos',
        'menu_icon'  => 'dashicons-products',
        'taxonomies'=> array( 'category' ),
        'supports' => array('title', 'thumbnail'),
        'show_in_nav_menus' => false,
      	'publicly_queryable' => false,
       );
      
      $argsExito = array(
         'public' => true,
         'label' => 'Caso de Exito',
         'taxonomies'=> array( 'exito-category' ),
         'menu_icon'  => 'dashicons-star-half',
         'supports'=> array('title', 'thumbnail'),
         'show_in_nav_menus' => true,
       );
      
      $argsAgenda = array(
        'public' => true,
        'label' => 'Agenda Tecnológica',
        'taxonomies'=> array( 'agenda-category' ),
        'menu_icon'  => 'dashicons-phone',
        'supports'=> array('title', 'editor', 'thumbnail'),
        'show_in_nav_menus' => true,
       );

 register_post_type( 'Slides', $argsSlide );
 register_post_type( 'Productos', $argsProducto );
 register_post_type( 'CasosExito', $argsExito );
 register_post_type( 'AgendaTel', $argsAgenda );
 register_post_type( 'Automatas', $argsRobot );

  remove_post_type_support( 'page', 'editor' );
  remove_post_type_support( 'page', 'thumbnail' );
  remove_post_type_support( 'page', 'revisions' );
  remove_post_type_support( 'page', 'comments' );

 

 add_theme_support('post-thumbnails');
 set_post_thumbnail_size( 140, 140, false );
 add_image_size( 'success-thumb', 696, 391, true );
 
}

function exito_custom_taxonomy() {

    register_taxonomy(
        'exito-category',
        'casosexito',
        array(
            'label' => __( 'Categorias' ),
            'rewrite' => array( 'slug' => 'exito-category' ),
            'hierarchical' => true,
            'show_in_nav_menus' => false,
        )
    );
}

function robot_custom_taxonomy() {

    register_taxonomy(
        'robot-category',
        'automatas',
        array(
            'label' => __( 'Categorias' ),
            'rewrite' => array( 'slug' => 'robot-category' ),
            'hierarchical' => true,
            'show_in_nav_menus' => false,
        )
    );
}


function agenda_custom_taxonomy() {

    register_taxonomy(
        'agenda-category',
        'agendatel',
        array(
            'label' => __( 'Categorias' ),
            'rewrite' => array( 'slug' => 'agenda-category' ),
            'hierarchical' => true,
            'show_in_nav_menus' => false
        )
    );
}

?>