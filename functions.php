<?php
    require get_template_directory() . '/inc/admin/admin-page.php';
    require get_template_directory() . '/inc/custom-post/cpt.php';
    require get_template_directory() . '/inc/menus/menu.php';
    require get_template_directory() . '/inc/meta-boxes/meta-boxes.php';
    // Si se desea agregar scripts hacerlo en  /inc/header/head.tpl.php
?>