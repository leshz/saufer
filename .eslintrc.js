module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "commonjs": true,
        "es6": true,
        "jquery": true
    },
    "extends": "airbnb-base",
    "settings": {
        "import/resolver": {
            "webpack": {
                "config": "./webpack.dev.js"
            }
        }
    },

    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly",
        "jquery": true

    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "rules": {
        "no-restricted-syntax": [
            "error",
            {
                "selector": "FunctionExpression",
                "message": "Function expressions are not allowed."
            },
            {
                "selector": "CallExpression[callee.name='setTimeout'][arguments.length!=2]",
                "message": "setTimeout must always be invoked with two arguments."
            }
        ]
    }
};