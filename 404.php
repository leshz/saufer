<?php get_header(); ?>

<style>
.no-found {
    width: 100%;
    margin: 65px auto;
    max-width: 1200px;
}
	
	img {
    width: 100%;
    height: auto;
    object-fit: contain;
}
</style>

<div class="no-found">
	<img src="<?php echo get_template_directory_uri();?>/static/images/404.png" alt="pagina 404">
</div>

<?php get_footer('agenda')?>