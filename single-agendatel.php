<?php get_header('agenda'); ?>

<body>
    
        <div class="ui container section-schedule shadow">
            <article>
                <h1><?php the_title(); ?> </h1>
                <h3><?php echo get_field('subtituloagenda');  ?></h3>
                <?php
                
                    $video = get_field('videoagenda');
                    $imagen = get_field('banneragenda');
                
                 if ( $video != null || $imagen != null ){ ?>

                    <div class="banner <?php echo ($video) ? 'contt' : '' ; ?>">
                    <?php if ($video) {?>
                        <div class="banner-video">
                            <?php echo $video;  ?>
                        </div>
                    
                        <?php }elseif( $imagen  != null){ ?>
                        <div class="banner-imagen">
                            <figure> <img src="<?php echo $imagen; ?>"></figure>
                        </div>
                    <?php } ?>
                    </div>
                 <?php } ?>
        
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
                <?php endwhile; endif; ?>
            </article>
        <section>	
             <?php $argsForAgenda = array(
            'post_type' => 'AgendaTel',
            'posts_per_page' => 5,
            'nopaging'=> false,
            'orderby'=> 'ID',
            'order'=> 'DESC',);
            
            $agenda_query = null;
            $agenda_query = new WP_Query($argsForAgenda);
            
            if ($agenda_query->have_posts()){?>
        <section>
                <div class="schedule-slider">
                    <h3>Le puede interesar</h3>
                    <div class="ui grid equal width tree center aligned padded start-slider">
                        
                        <?php while ($agenda_query->have_posts()) : $agenda_query->the_post(); ?>
                        <div class="column">
                            <div class="schedule-slider-container">
                                <div class="image-to-post">
                                    <picture><?php echo the_post_thumbnail('large') ?></picture>
                                </div>
                                <div class="content">
                                    <h3 class="title"><?php echo the_title();?></h3>
                                     <?php echo the_content('Leer más') ?>
                                </div>
                            </div>
                        </div>
                        <?php endwhile;?>
                    </div>
                </div>
            </section>
            <?php }?>
        </div>
    </section>
<?php get_footer('agenda')?>

