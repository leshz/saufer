const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: {
    index: path.resolve(__dirname, "src/main-entry.js"),
    products: path.resolve(__dirname, "src/products.js"),
    agenda: path.resolve(__dirname, "src/agenda.js"),
    exito: path.resolve(__dirname, "src/exito.js")
  },
  devtool: "source-map",
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        include: path.resolve(__dirname, "src"),
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      },
      {
        test: /\.(woff(2)?|ttf|otf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "static/fonts/"
              // publicPath: '../static/fonts/',
            }
          }
        ]
      },
      {
        test: /\.(png|jpg|jpeg|gif|svg)$/i,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 3000, // in bytes
              name: "[name].[ext]",
              outputPath: "static/images/"
            }
          }
        ]
      },
      {
        test: /\.(scss|css)$/,
        use: [
          {
            loader: "css-hot-loader"
          },
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader"
          },
          {
            loader: "postcss-loader",
            options: {
              ident: "postcss"
            }
          },
          {
            loader: "sass-loader"
          }
        ]
      },
      {
        test: require.resolve("jquery"),
        use: "imports-loader?this=>window"
      },
      {
        test: require.resolve("fullpage.js"),
        use: "imports-loader?this=>window"
      },
      {
        test: /\.pug$/,
        use: ["pug-loader"]
      }
    ]
  },
  resolve: {
    extensions: ["*", ".js", ".jsx"],
    alias: {
      Sbase: path.resolve(__dirname, "src/scss/base"),
      Scomponents: path.resolve(__dirname, "src/scss/components"),
      Slayout: path.resolve(__dirname, "src/scss/layout"),
      Svendor: path.resolve(__dirname, "src/scss/vendor"),
      Stemplates: path.resolve(__dirname, "src/scss/templates")
    }
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "/css/[name].css"
    }),
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: "./src/templates/index.pug",
      // inject: true,
      chunks: ["index"]
    }),
    new HtmlWebpackPlugin({
      filename: "productos.html",
      template: "./src/templates/productos.pug",
      inject: true,
      chunks: ["products"]
    }),
    new HtmlWebpackPlugin({
      filename: "agenda.html",
      template: "./src/templates/agenda.pug",
      inject: true,
      chunks: ["agenda"]
    }),
    new HtmlWebpackPlugin({
      filename: "exito.html",
      template: "./src/templates/exito.pug",
      inject: true,
      chunks: ["exito"]
    })
  ],
  output: {
    path: path.resolve(__dirname, "./"),
    publicPath: "./",
    filename: "js/[name].js"
  },
  devServer: {
    contentBase: path.resolve(__dirname, "/"),
    publicPath: "/",
    compress: true,
    // port: 8080,
    watchContentBase: true
  },
  optimization: {
    splitChunks: {
      chunks: "all"
    }
  }
};
