<?php get_header('agenda'); ?>

<body>
    
        <div class="ui container section-schedule shadow">
            <article>
                <h1><?php the_title(); ?> </h1>
                <h3><?php echo get_field('subtitulorobot');  ?></h3>
                <?php
                
                    $video = get_field('videorobot');
                    $imagen = get_field('bannerrobot');
                
                 if ( $video != null || $imagen != null ){ ?>

                    <div class="banner <?php echo ($video) ? 'contt' : '' ; ?>">
                    <?php if ($video) {?>
                        <div class="banner-video">
                            <?php echo $video;  ?>
                        </div>
                    
                        <?php }elseif( $imagen  != null){ ?>
                        <div class="banner-imagen">
                            <figure> <img src="<?php echo $imagen; ?>"></figure>
                        </div>
                    <?php } ?>
                    </div>
                 <?php } ?>
        
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
                <?php endwhile; endif; ?>
                
                
                <?php 
                
                $taxonomy = get_post_taxonomies();
                $taxonomy= $taxonomy[0];
                $term = get_the_terms('' ,$taxonomy);
                $termOB = $term[0];
                $pepe = $termOB->slug;
        
                ?>
            </article>
            
            
        <section>	
             <?php 
             $argsForRobots = array(
            'post_type' => 'Automatas',
            'nopaging'=> false,
                        'taxonomy' => $taxonomy,
                        'term' => $pepe,
            'orderby'=> 'ID',
            'order'=> 'DESC',);
            
            $robots_query = null;
            $robots_query = new WP_Query($argsForRobots);
            
            if ($robots_query->have_posts()){?>
        <section>
                <div class="schedule-slider">
                    <h3>Le puede interesar</h3>
                    <div class="ui grid equal width tree center aligned padded start-slider">
                        
                        <?php while ($robots_query->have_posts()) : $robots_query->the_post(); ?>
                        <div class="column">
                            <div class="schedule-slider-container">
                                <div class="image-to-post">
                                    <picture><?php echo the_post_thumbnail('medium') ?></picture>
                                </div>
                                <div class="content">
                                    <h3 class="title"><?php echo the_title();?></h3>
                                     <?php echo the_content('Leer más') ?>
                                </div>
                            </div>
                        </div>
                        <?php endwhile;?>
                    </div>
                </div>
            </section>
            <?php }?>
        </div>
    </section>
<?php get_footer('agenda')?>

