<?php 
    get_header('exito');
    
?>

    <div class="ui container inside-pages success">
        <ui class="container">
            <?php 
            
            $papi = get_the_terms( get_the_ID(), 'exito-category' ) ;
            $bigpapi = $papi[0]->parent;
            $terms = get_categories( array(
                'title_li' => '',
                'style'    => 'none',
                'echo'     => false,
                'parent'   => $bigpapi,
                'taxonomy' => 'exito-category'
            ) );
            $cont = 0;
    ?>
            <div class="selector-ex"> 
                <?php foreach($terms as $term){?>
                <button class="selector-ex-button" data-tab=<?php echo $cont; ?>><?php echo $term -> name; ?></button>
                <?php
                $cont++;
                }?>
            </div>
            
            <?php foreach($terms as $term){?>
            <div class="ui three column grid shadow page-exito padded stackable">
                <?php
                $argsForExito = array(
                  'post_type' => 'CasosExito',
                  'taxonomy' => 'exito-category',
                  'term' => $term -> name,
                  'nopaging'=> false,
                  'orderby'=> 'ID',
                  'order'=> 'DESC',);
      
              $exito_query = null;
              $exito_query = new WP_Query($argsForExito);
              
            if ($exito_query->have_posts()){
                 while ($exito_query->have_posts()) : $exito_query->the_post();
                    $caseid = get_the_ID();
                    $meta = get_post_meta($caseid, 'arrayCaracteristicasExito', TRUE );
                    $metaSlide = get_post_meta($caseid, 'arraySlideCases', TRUE );
                    $meta = json_decode($meta,true);
                    $metaSlide = json_decode($metaSlide,true);
            ?>
                
                <div class="column">
                    <div class="card-success">
                        <div class="card-success-image"><img class="float" src="<?php echo get_the_post_thumbnail_url(null, 'medium') ?>"></div>
                        <div class="card-success-content">
                            <h3><?php echo the_title(); ?></h3>
                            <p><?php echo get_field('descripcioncaso'); ?></p>
                        </div>
                        <div class="card-success-modal-content">
                            <button class="close-modal">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABGCAMAAABG8BK2AAAAilBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATAggvAAAALXRSTlMAA/z4IF3XEhr0gmks6cWp8JV0V1IwC97ObztaHuTb06skybR6ubivhEMVnECnnbubAAADKklEQVRYw7yV6ZaiMBCFRwIS2QRkc197m27e//WmKjHeAHpg5sfEc9Tc3PqsKiv663+smbX+HTEiTIGoJxkVYR4EeVhEUit/DZGrLInbx4qTbLUDaBqkWi0XKtpxHCHoSW0Wp20F0CjEXx84iuNNMkzj10Puk2UKZbbek98g0riu49SgSNuvZ1M4m0SVwjUkn0XpSt+Xbll8JlSlPkg24wXlQnvTtzDqHkbhW6pB4qasryn+BxuppOz8GD/zQuucCX1+/Q3OkOI2rUOPdlm+Gr9y2SrL8QxOn3KeK0vzxTFDg1a/9EcdInC6lN2BjimVCpBnoIoSImPtgtPpS6Mowchc0GGgOEf0xz79YIrwpoyoJ5hzBQaUXJXsTRtRT5lvxoyDjeCDYIxi7AFzxOZuR+cSnocl5DHOkv2XatZVQ1YbH+oYx284Yt39XH/P/bVy7F0/iOgCdzn2O8drlGTLr7YoK7flqibywp5L7/ot9RZx8vvqWdtowUNYWcqKwRm9McIPDdjcZQEmd07ij2XKOGplCadWtKlrCbdWiHa+s3uxm5PGowJuSmEneCSn927HlDymOh/kwkNe2q53boVETRwT0hs4tg44oDjbjilkjSRTJBqMcacp1RxDIQFXBU3OHhlfqMZkpnfgPPIxuYCC0RftxSQjY9ymZ5xnFNysWLLMubG9gKdXl5SoqOcoGE9dNxvnvsFCn49HdLdnKDmSEzANx9QM6xpWhMlRX7HG5KhwwBH3P5wBBV3N75iA+l3jqvbuLEHwg9Bbfk2hwWsM/rg0pvlTixmjQAzDQNCQgIuUqQP5/yvv4DhGiZhqscsQDLY8uytx5r6NHwqOfgu+5FB6xRAAF3bFXnAIOE+4kIL78ysEFC7k+SkMlaPKl8AgaFam37x3NJtQUCNorJxSryoUJltjwtGTr8k/yJaK6MGJnrwfKqJV0rH672KX8s0lHYPBuO7t+lsHf13bjS0Vg3G7G/vsr23uw+zOzbczZObrUcDyYYsCC4JJGpPy0LYgQuaBNo/Xedhf1nrkjVDeluVNYt6y5g30knY+Hy7ko4588JKPgfKh1Pr1AWlzg39+zLc+AAAAAElFTkSuQmCC"></button>
                            <div class="imagen-content">
                                <?php if (get_field('videocaso') != null) {?>
                                    <?php echo get_field('videocaso');  ?>
                                <?php }elseif( get_field('bannercaso') != null){ ?>
                                    <img src="<?php echo get_field('bannercaso'); ?>" alt="" srcset="">
                                <?php }else {} ?>
                            </div>
                            <?php if (count($metaSlide) > 0 ){ ?>
                            <div class="slider">
                                <div class="slider-wrap">
                                    <?php foreach ($metaSlide as $item){ ?>
                                    <div class="slide-item">
                                        <img src="<?php echo wp_get_attachment_image_url($item['imagen'],'Full size')?>" alt="" srcset="">
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="info-card">
                                <p>Cliente:<span><?php echo get_field('clientecaso'); ?></span></p>
                                <p>Fecha de instalacion:<span><?php echo get_field('fechacaso'); ?></span></p>
                                <p>Ubicacion:<span><?php echo get_field('ubicacioncaso'); ?></span></p>
                                <p>Descripcion:</p>
                                <p><?php echo get_field('descripcioncaso'); ?></p>
                            </div>
                            <?php if (count($meta) > 0 ){ ?>
                            <div class="icons-description">
                                <?php foreach ($meta as $item){ ?>
                                <div class="item">
                                    <div class="img-cont"><img src="<?php echo wp_get_attachment_image_url($item['imagen'],'Full size')?>"></div>
                                    <div class="description"><span><?php echo $item['description'];?> </span></div>
                                </div>
                                <?php } ?>
                            </div>
                            <?php } ?>
                            <?php
                                $link = get_field('linkasesoria');
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                            <div class="call-to-action"><a href="<?php echo esc_url($link_url); ?>" target = "<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title) ?></a></div>
                        </div>
                    </div>
                </div>
                <?php 
                endwhile;
                }
                ?>
                
            </div>
            <?php
                $cont++;
            }?>
        </ui>
    </div>
<?php get_footer('exito')?>